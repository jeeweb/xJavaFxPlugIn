package com.xwintop.xJavaFxPlugIn.view;

import com.xwintop.xJavaFxPlugIn.model.IDCardToolTableBean;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;

public abstract class IDCardToolView implements Initializable {

    @FXML
    protected TextField textFileOriginalPath;
    @FXML
    protected TextField textFileTargetPath;
    @FXML
    protected TextField textFileNamingRules;
    @FXML
    protected ComboBox comboBoxScanningRules;
    @FXML
    protected TextField textOcrAppId;
    @FXML
    protected TextField textOcrApiKey;
    @FXML
    protected TextField textOcrsecretKey;
    @FXML
    protected Button buttonChooseOriginalPath;
    @FXML
    protected Button buttonChooseTargetPath;
    @FXML
    protected Button buttonCopy;
    @FXML
    protected Spinner<Integer> spinnerCopyNumber;
    @FXML
    protected TableView<IDCardToolTableBean> tableViewMain;
    @FXML
    protected TableColumn<IDCardToolTableBean, String> tableColumnFileOriginalPath;
    @FXML
    protected TableColumn<IDCardToolTableBean, String> tableColumnFileTargetPath;
    @FXML
    protected TableColumn<IDCardToolTableBean, String> tableColumnFileNumber;
    @FXML
    protected TableColumn<IDCardToolTableBean, String> tableColumnOcrAppId;
    @FXML
    protected TableColumn<IDCardToolTableBean, String> tableColumnOcrApiKey;
    @FXML
    protected TableColumn<IDCardToolTableBean, String> tableColumnOcrsecretKey;
    @FXML
    protected TableColumn<IDCardToolTableBean, String>  tableColumnlineNumber;
    @FXML
    protected TableColumn<IDCardToolTableBean, String> tableColumnUserName;
    @FXML
    protected TableColumn<IDCardToolTableBean, String> tableColumnUserIdCardNumber;
    @FXML
    protected TableColumn<IDCardToolTableBean, String> tableColumnUserAddress;
    @FXML
    protected TableColumn<IDCardToolTableBean, String> tableColumnUserNation;
    @FXML
    protected TableColumn<IDCardToolTableBean, String> tableColumnUserSex;
    @FXML
    protected TableColumn<IDCardToolTableBean, String> tableColumnUserBorn;
    @FXML
    protected TableColumn<IDCardToolTableBean, String>tableColumnSignOrg;
    @FXML
    protected TableColumn<IDCardToolTableBean, String>tableColumnExpiryDate;
    @FXML
    protected TableColumn<IDCardToolTableBean, String>tableColumnSignDate;
    @FXML
    protected CheckBox checkBoxIsOneDirectory;
    @FXML
    protected CheckBox checkBoxIsMerge;
    @FXML
    protected Button buttonAddItem;
    @FXML
    protected Button buttonSaveConfigure;
    @FXML
    protected Button buttonDeleteSelectRow;
    @FXML
    protected Button otherSaveConfigureButton;
    @FXML
    protected Button loadingConfigureButton;
    @FXML
    protected Button runQuartzButton;
    @FXML
    protected AnchorPane simpleScheduleAnchorPane;
    @FXML
    protected Spinner<Integer> intervalSpinner;
    @FXML
    protected Spinner<Integer> repeatCountSpinner;


}
