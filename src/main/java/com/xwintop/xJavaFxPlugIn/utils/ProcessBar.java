package com.xwintop.xJavaFxPlugIn.utils;

import javafx.scene.control.ProgressIndicator;

import javax.swing.*;
import java.awt.*;

/**
 * @Auther: Administrator
 * @Date: 2018/6/21 17:21
 * @Description:
 */
public class ProcessBar extends JFrame implements Runnable {
    /**
     *   进度条
      */
    JProgressBar progress;
    ProgressIndicator ploading;
    int currentNumber = 0;
    int totalNumber = 0;
    int process = 0;

    public ProcessBar(String str) {
        super(str);
        // 实例化进度条
        progress = new JProgressBar(1, 100);
        ploading = new ProgressIndicator();
        // 描绘文字
        progress.setStringPainted(true);
        // 设置背景色
        progress.setBackground(Color.PINK);

        this.add(progress);
        this.setBounds(200, 300, 250, 50);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setAlwaysOnTop(true);
    }

    @Override
    public void run() {
        while (true) {
            while (process < 100) {
                try {
                    if (currentNumber == 0) {
                        process = 0;
                    } else {
                        System.out.println("currentNumber:" + currentNumber + ",totalNumber:" + totalNumber);
                        process = (int) (((float) currentNumber / totalNumber) * 100);
                        System.out.println("process" + process);
                    }
                    // 随着线程进行，增加进度条值
                    progress.setValue(process);
                    progress.setString(process + "%");
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            JOptionPane.showMessageDialog(this, "识别完成!");
            this.dispose();
            break;
        }

    }

    public int getProcess() {
        return process;
    }

    public void setProcess(int process) {
        this.process = process;
    }

    public void setVisible() {
        this.setVisible(true);
    }

    public int getCurrentNumber() {
        return currentNumber;
    }

    /**
     * 设置自增
     */
    public void setCurrentNumber(){
      this.currentNumber++;
    }

    public void setCurrentNumber(int currentNumber) {
        this.currentNumber = currentNumber;
    }

    public int getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(int totalNumber) {
        this.totalNumber = totalNumber;
    }
}
