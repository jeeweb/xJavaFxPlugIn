package com.xwintop.xJavaFxPlugIn.utils;

import com.baidu.aip.ocr.AipOcr;

public class BaiduOcrUtil {

    public static String appId;
    public static String apiKey;
    public static String SecretKey;

    public static String getAppId() {
        return appId;
    }

    public static void setAppId(String appId) {
        BaiduOcrUtil.appId = appId;
    }

    public static String getApiKey() {
        return apiKey;
    }

    public static void setApiKey(String apiKey) {
        BaiduOcrUtil.apiKey = apiKey;
    }

    public static String getSecretKey() {
        return SecretKey;
    }

    public static void setSecretKey(String secretKey) {
        SecretKey = secretKey;
    }

    private static AipOcr client = null;

    public static AipOcr client() {
        if (client == null) {
            client = new AipOcr(appId, apiKey, SecretKey);
            client.setConnectionTimeoutInMillis(2000);
            client.setSocketTimeoutInMillis(5000);
            return client;
        }
        return client;
    }

}
