package com.xwintop.xJavaFxPlugIn.utils;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import javax.imageio.ImageIO;

public class CompositePhotograph
{
    /**
     * @描述: 合成图片
     * @param face 身份证正面
     * @param back 身份证反面
     * @param fileName 合成图片文件名
     * @return
     * @throws IOException
     */
    public static File compositePhoto(File face , File back , String fileName, Path path) throws IOException{
        BufferedImage b1 = ImageIO.read(face);
        BufferedImage b2 = ImageIO.read(back);
        //根据两张图片得到合成图片的宽高
        int width = b1.getWidth()<b2.getWidth() ? b1.getWidth() : b2.getWidth();
        int height = b1.getHeight() + b2.getHeight();
        //合成的图片
        BufferedImage cp = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics g = cp.getGraphics();
        //上部分
        g.drawImage(b1,0,0,null);
        //下部分
        g.drawImage(b2,0,b1.getHeight(),null);
        //检查文件
        File f = new File(path.toString());
        if(f.exists()){
            f.delete();
            f.createNewFile();
        }else{
            f.createNewFile();
        }
        //输出图片
        ImageIO.write(cp, "jpg", f);
        return f;
    }

    /**
     * @描述: 改变图片尺寸，按给出的width，等比例缩放高
     * @param fileName 生成的新图片的文件名
     * @param width 修改的宽
     * @return
     * @throws IOException
     */
    public static File resize(File source,String fileName,int width,Path path) throws IOException{
        //原图片文件
        Image img = ImageIO.read(source);
        int srcW = img.getWidth(null);
        int srcH = img.getHeight(null);
        //缩放比例
        double x = (double)width/(double)srcW;
        //缩放后的高
        int height = (int) (srcH * x);
        //新图片
        BufferedImage newImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        //缩放后的图片
        Image tag = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        //绘制到画板上
        newImg.getGraphics().drawImage(tag, 0, 0, null);
        File f = new File(path.toString());
        if(f.exists()){
            f.delete();
            f.createNewFile();
        }else{
            f.createNewFile();
        }
        //写入文件
        ImageIO.write(newImg, "jpg", f);
        return f;
    }
}