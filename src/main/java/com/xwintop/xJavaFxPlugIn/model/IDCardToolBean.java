package com.xwintop.xJavaFxPlugIn.model;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

public class IDCardToolBean {
    //文件存在路径
    private SimpleStringProperty fileOriginalPath;
    //文件存储路径
    private SimpleStringProperty fileTargetPath;
    //文件命名规则
    private SimpleStringProperty fileNamingRules;
    //处理文件数量
    private SimpleStringProperty fileNumber;
    //appId
    private SimpleStringProperty ocrAppId;
    //apiId
    private SimpleStringProperty ocrApiKey;
    //Secret_Key
    private SimpleStringProperty ocrsecretKey;
    //身份证正反面是否同目录
    private SimpleBooleanProperty isOneDirectory;
    //图片正反面扫描规则 _* 全部相同,*_ 前面相同, _*_中间相同,后面相同_*
    private SimpleStringProperty scanningRules;
    //是否合并图片
    private SimpleBooleanProperty isMerge;

    public IDCardToolBean(String fileOriginalPath, String fileTargetPath, String ocrAppId, String ocrApiKey, String ocrsecretKey, String fileNamingRules, Boolean isOneDirectory, String scanningRules, Boolean isMerge) {
        super();
        this.fileOriginalPath = new SimpleStringProperty(fileOriginalPath);
        this.fileTargetPath = new SimpleStringProperty(fileTargetPath);
        this.ocrAppId = new SimpleStringProperty(ocrAppId);
        this.ocrApiKey = new SimpleStringProperty(ocrApiKey);
        this.ocrsecretKey = new SimpleStringProperty(ocrsecretKey);
        this.fileNamingRules = new SimpleStringProperty(fileNamingRules);
        this.isOneDirectory = new SimpleBooleanProperty(isOneDirectory);
        this.scanningRules = new SimpleStringProperty(scanningRules);
        this.isMerge = new SimpleBooleanProperty(isMerge);
    }

    public IDCardToolBean(String propertys) {
        String[] strings = propertys.split("__", 9);
        this.fileOriginalPath = new SimpleStringProperty(strings[0]);
        this.fileTargetPath = new SimpleStringProperty(strings[1]);
        this.ocrAppId = new SimpleStringProperty(strings[2]);
        this.ocrApiKey = new SimpleStringProperty(strings[3]);
        this.ocrsecretKey = new SimpleStringProperty(strings[4]);
        this.fileNamingRules = new SimpleStringProperty(strings[5]);
        this.isOneDirectory = new SimpleBooleanProperty(Boolean.valueOf(strings[6]));
        this.scanningRules = new SimpleStringProperty(strings[7]);
        this.isMerge = new SimpleBooleanProperty(Boolean.valueOf(strings[8]));
    }

    public String getPropertys() {
        return fileOriginalPath.get() + "__" + fileTargetPath.get() + "__"
                + ocrAppId.get() + "__" + ocrApiKey.get() + "__" + ocrsecretKey.get() + "__" + fileNamingRules.get() + "__" + isOneDirectory.get() + "__" + scanningRules.get() + "__" + isMerge.get();
    }

    public String getFileOriginalPath() {
        return fileOriginalPath.get();
    }

    public SimpleStringProperty fileOriginalPathProperty() {
        return fileOriginalPath;
    }

    public void setFileOriginalPath(String fileOriginalPath) {
        this.fileOriginalPath.set(fileOriginalPath);
    }

    public String getFileTargetPath() {
        return fileTargetPath.get();
    }

    public SimpleStringProperty fileTargetPathProperty() {
        return fileTargetPath;
    }

    public void setFileTargetPath(String fileTargetPath) {
        this.fileTargetPath.set(fileTargetPath);
    }

    public String getFileNumber() {
        return fileNumber.get();
    }

    public SimpleStringProperty fileNumberProperty() {
        return fileNumber;
    }

    public void setFileNumber(String fileNumber) {
        this.fileNumber.set(fileNumber);
    }

    public String getOcrAppId() {
        return ocrAppId.get();
    }

    public SimpleStringProperty ocrAppIdProperty() {
        return ocrAppId;
    }

    public void setOcrAppId(String ocrAppId) {
        this.ocrAppId.set(ocrAppId);
    }

    public String getOcrApiKey() {
        return ocrApiKey.get();
    }

    public SimpleStringProperty ocrApiKeyProperty() {
        return ocrApiKey;
    }

    public void setOcrApiKey(String ocrApiKey) {
        this.ocrApiKey.set(ocrApiKey);
    }

    public String getOcrsecretKey() {
        return ocrsecretKey.get();
    }

    public SimpleStringProperty ocrsecretKeyProperty() {
        return ocrsecretKey;
    }

    public void setOcrsecretKey(String ocrsecretKey) {
        this.ocrsecretKey.set(ocrsecretKey);
    }

    public String getFileNamingRules() {
        return fileNamingRules.get();
    }

    public SimpleStringProperty fileNamingRulesProperty() {
        return fileNamingRules;
    }

    public void setFileNamingRules(String fileNamingRules) {
        this.fileNamingRules.set(fileNamingRules);
    }

    public boolean isIsOneDirectory() {
        return isOneDirectory.get();
    }

    public SimpleBooleanProperty isOneDirectoryProperty() {
        return isOneDirectory;
    }

    public void setIsOneDirectory(boolean isOneDirectory) {
        this.isOneDirectory.set(isOneDirectory);
    }

    public String getScanningRules() {
        return scanningRules.get();
    }

    public SimpleStringProperty scanningRulesProperty() {
        return scanningRules;
    }

    public void setScanningRules(String scanningRules) {
        this.scanningRules.set(scanningRules);
    }

    public boolean isIsMerge() {
        return isMerge.get();
    }

    public SimpleBooleanProperty isMergeProperty() {
        return isMerge;
    }

    public void setIsMerge(boolean isMerge) {
        this.isMerge.set(isMerge);
    }
}
