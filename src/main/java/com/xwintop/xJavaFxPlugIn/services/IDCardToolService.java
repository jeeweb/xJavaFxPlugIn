package com.xwintop.xJavaFxPlugIn.services;

import com.baidu.aip.ocr.AipOcr;
import com.xwintop.xJavaFxPlugIn.model.IDCardToolBean;
import com.xwintop.xJavaFxPlugIn.model.IDCardToolTableBean;
import com.xwintop.xJavaFxPlugIn.utils.BaiduOcrUtil;
import com.xwintop.xJavaFxPlugIn.utils.CompositePhotograph;
import com.xwintop.xJavaFxTool.utils.ConfigureUtil;
import com.xwintop.xJavaFxPlugIn.utils.ProcessBar;
import com.xwintop.xcore.util.javafx.FileChooserUtil;
import com.xwintop.xcore.util.javafx.TooltipUtil;
import javafx.stage.FileChooser;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IDCardToolService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private IDCardToolBean toolData;
    private SchedulerFactory sf = new StdSchedulerFactory();
    /*  private String schedulerKeyGroup = "runFileCopy";
      private String schedulerKeyName = "runFileCopy" + System.currentTimeMillis();*/
    private MimetypesFileTypeMap mtftp;

    public void saveConfigure() throws Exception {
        saveConfigure(ConfigureUtil.getConfigureFile("idCardConfigure.properties"));
    }

    public void saveConfigure(File file) throws Exception {
        FileUtils.touch(file);
        PropertiesConfiguration xmlConfigure = new PropertiesConfiguration(file);
        xmlConfigure.clear();
        xmlConfigure.setProperty("tableBean", toolData.getPropertys());
        xmlConfigure.save();
        //TooltipUtil.showToast("保存配置成功,保存在：" + file.getPath());
    }

    public void otherSaveConfigureAction() throws Exception {
        String fileName = "idCardConfigure.properties";
        File file = FileChooserUtil.chooseSaveFile(fileName, new FileChooser.ExtensionFilter("All File", "*.*"),
                new FileChooser.ExtensionFilter("Properties", "*.properties"));
        if (file != null) {
            saveConfigure(file);
            // TooltipUtil.showToast("保存配置成功,保存在：" + file.getPath());
        }
    }

    public IDCardToolBean loadingConfigure() {
        loadingConfigure(ConfigureUtil.getConfigureFile("idCardConfigure.properties"));
        return toolData;
    }

    public IDCardToolBean loadingConfigure(File file) {
        try {

            PropertiesConfiguration xmlConfigure = new PropertiesConfiguration(file);
            xmlConfigure.getKeys().forEachRemaining(new Consumer<String>() {
                @Override
                public void accept(String t) {
                    toolData = new IDCardToolBean(xmlConfigure.getString(t));
                }
            });
        } catch (Exception e) {
            try {
                TooltipUtil.showToast("加载配置失败：" + e.getMessage());
            } catch (Exception e2) {
            }
        }
        return toolData;
    }

    public IDCardToolBean loadingConfigureAction() {
        File file = FileChooserUtil.chooseFile(new FileChooser.ExtensionFilter("All File", "*.*"),
                new FileChooser.ExtensionFilter("Properties", "*.properties"));
        if (file != null) {
            toolData = loadingConfigure(file);
        }
        return toolData;
    }

    public List<IDCardToolTableBean> idCardRunAction(ProcessBar pb) throws Exception {
        mtftp = new MimetypesFileTypeMap();
        /* 不添加下面的类型会造成误判 详见：http://stackoverflow.com/questions/4855627/java-mimetypesfiletypemap-always-returning-application-octet-stream-on-android-e*/
        mtftp.addMimeTypes("image png tif jpg jpeg bmp");
        List<IDCardToolTableBean> tableBeans = new ArrayList<>();
        IDCardToolBean toolBean = toolData;
        BaiduOcrUtil.setAppId(toolBean.getOcrAppId());
        BaiduOcrUtil.setApiKey(toolBean.getOcrApiKey());
        BaiduOcrUtil.setSecretKey(toolBean.getOcrsecretKey());
        HashMap<String, String> options = new HashMap<String, String>();
        //获取文件
        File file = new File(toolBean.getFileOriginalPath());
        File[] tempList = file.listFiles();
        try {

            AipOcr client = BaiduOcrUtil.client();
            //同目录处理逻辑

            System.out.println("该目录下对象个数：" + tempList.length);
            //int lineNumber = 0;
            //设置总数量
            //文件命名 *判断相同位置  *_.jpg ,*.jpg,_*.jpg
            pb.setTotalNumber(tempList.length);
            Map<String, List<IDCardToolTableBean>> listMap = new HashMap();

            ForkJoinPool pool = new ForkJoinPool();
            List<File> fList = Arrays.asList(tempList);
            int handleType;
            if (!toolBean.isIsOneDirectory()) {
                //不递归
                handleType = 1;
            } else {
                //递归
                handleType = 2;
            }
            FliesHandleTask innerFind = new FliesHandleTask(fList, 0, tempList.length - 1, toolBean, client, listMap, handleType, pb, tableBeans);

            long start = System.currentTimeMillis();
            //同步调用
            pool.invoke(innerFind);
            logger.info("开始执行多线程");
            int sucessCount = innerFind.join();
            pb.setCurrentNumber(sucessCount);
            if (tempList.length == sucessCount) {
                logger.info("多线程处理文件成功");
                //遍历Map
                if (!toolBean.isIsOneDirectory()) {
                    for (Map.Entry<String, List<IDCardToolTableBean>> entry : listMap.entrySet()) {
                        //存储对象进tables里面
                        if (entry.getValue() != null) {
                            IDCardToolTableBean idBean = saveBeans(entry.getValue(), toolBean);
                            tableBeans.add(idBean);
                        }
                    }
                }

            } else {
                logger.info("多线程处理文件失败");
            }
            logger.info("结束多线程处理文件,spend time:" + (System.currentTimeMillis() - start) + "ms");

        } catch (Exception e) {
            pb.setCurrentNumber(tempList.length);
            e.printStackTrace();
        } finally {
            return tableBeans;
        }

    }

    public IDCardToolBean getToolData() {
        return toolData;
    }

    public void setToolData(IDCardToolBean toolData) {
        this.toolData = toolData;
    }

    /**
     * 判断文件是否是图片
     *
     * @param file
     * @return
     */
    public boolean isImage(File file) {

        String mimetype = mtftp.getContentType(file);
        String type = mimetype.split("/")[0];
        return type.equals("image");
    }

    public JSONObject getJSONObject(JSONObject jsonObject, String fileStr) {
        if (jsonObject == null) {
            return null;
        }
        if (jsonObject.has(fileStr)) {
            return jsonObject.getJSONObject(fileStr);
        } else {
            return null;
        }
    }

    /**
     * 处理照片逻辑
     *
     * @param tempList
     * @param i
     */
    private IDCardToolTableBean PicProcessing(File[] tempList, int i, AipOcr client) throws Exception {
        String signOrgV = "", expiryDateV = "", signDateV = "", nameV = "", numberV = "", address = "", nationV = "", bornV = "", sexV = "", namingRules = "", idCardSign = "";
        HashMap<String, String> options = new HashMap<String, String>();
        String fileOriginalPath = tempList[i].getPath().toString();
        options.put("detect_direction", "true");
        //front：身份证正面；back：身份证背面
        JSONObject response = client.idcard(tempList[i].getPath(), "front", options);
        if (!response.isNull("error_code")) {
            //识别失败
            //进行反面识别
            JSONObject backResponse = client.idcard(tempList[i].getPath(), "back", options);
            JSONObject idBackCardObject = getJSONObject(backResponse, "words_result");
            JSONObject signOrgObject = getJSONObject(idBackCardObject, "签发机关");
            JSONObject expiryDateObject = getJSONObject(idBackCardObject, "失效日期");
            JSONObject signDateObject = getJSONObject(idBackCardObject, "签发日期");
            if (!backResponse.isNull("error_code")) {
                //正面背面都识别失败
            } else {
                idCardSign = "back";
                signOrgV = signOrgObject != null ? signOrgObject.getString("words") : "";
                expiryDateV = expiryDateObject != null ? expiryDateObject.getString("words") : "";
                signDateV = signDateObject != null ? signDateObject.getString("words") : "";
            }
        } else {
            idCardSign = "front";
            JSONObject idCardObject = getJSONObject(response, "words_result");
            JSONObject nameObject = getJSONObject(idCardObject, "姓名");
            JSONObject numberObject = getJSONObject(idCardObject, "公民身份号码");
            JSONObject addressObject = getJSONObject(idCardObject, "住址");
            JSONObject nationObject = getJSONObject(idCardObject, "民族");
            JSONObject bornObject = getJSONObject(idCardObject, "出生");
            JSONObject sexObject = getJSONObject(idCardObject, "性别");
            nameV = nameObject != null ? nameObject.getString("words") : "";
            numberV = numberObject != null ? numberObject.getString("words") : "";
            address = addressObject != null ? addressObject.getString("words") : "";
            nationV = nationObject != null ? nationObject.getString("words") : "";
            bornV = bornObject != null ? bornObject.getString("words") : "";
            sexV = sexObject != null ? sexObject.getString("words") : "";


        }
        String fileName = tempList[i].getName();
        String prefix = fileName.substring(fileName.lastIndexOf("."));
        return new IDCardToolTableBean("", fileOriginalPath, prefix, nameV, address, numberV, nationV, sexV, bornV, idCardSign, signOrgV, expiryDateV, signDateV, namingRules, tempList[i]);
    }


    private String copyFileF(String fileTargetPath, String namingRules, String idCardSign, String prefix, File originalFile) throws Exception {
        //拼接文件名称
        String regEx = "[\\s~·`!！@#￥$%^……&*（()）\\-——\\-_=+【\\[\\]】｛{}｝\\|、\\\\；;：:‘'“”\"，,《<。.》>、/？?]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(namingRules);
        m.replaceAll("_");
        String targetFilePath = fileTargetPath + File.separator + namingRules + "_" + idCardSign + prefix;
        File targetFile = new File(targetFilePath);
        //若文件存在重命名
        int fileNumber = 1;
        while (targetFile.exists()) {
            String newFilename = namingRules + "(" + fileNumber + ")" + "_" + idCardSign + prefix;
            String parentPath = targetFile.getParent();
            targetFile = new File(parentPath + File.separator + newFilename);
            fileNumber++;
        }
        FileUtils.copyFile(originalFile, targetFile);
        return targetFile.getPath();
    }


    private IDCardToolTableBean saveBeans(List<IDCardToolTableBean> tableBeanList, IDCardToolBean toolBean) throws Exception {
        Map<String, String> map = new HashMap<>();
        File zmFile = null;
        File fmFile = null;
        for (IDCardToolTableBean tb : tableBeanList) {
            if (tb.getIdCardSign().equals("front")) {
                map.put("zmIdCardSign", tb.getIdCardSign());
                map.put("zmFileOriginalPath", tb.getFileOriginalPath());
                map.put("zmFileTargetPath", tb.getFileTargetPath());
                map.put("userName", tb.getUserName());
                map.put("userAddress", tb.getUserAddress());
                map.put("userIdCardNumber", tb.getUserIdCardNumber());
                map.put("userNation", tb.getUserNation());
                map.put("userSex", tb.getUserSex());
                map.put("userBorn", tb.getUserBorn());
                map.put("namingRules", tb.getNamingRules());
                zmFile = tb.getOriginalFile();
            } else {
                fmFile = tb.getOriginalFile();
                map.put("fmIdCardSign", tb.getIdCardSign());
                map.put("fmFileOriginalPath", tb.getFileOriginalPath());
                map.put("fmFileTargetPath", tb.getFileTargetPath());
                map.put("fileTargetPath", tb.getFileTargetPath());
                map.put("signOrg", tb.getSignOrg());
                map.put("expiryDate", tb.getExpiryDate());
                map.put("signDate", tb.getSignDate());
            }
        }

        String namingRules = toolBean.getFileNamingRules();
        //替换占位符
        namingRules = namingRules.replace("{name}", map.get("userName") != null ? map.get("userName") : "");
        namingRules = namingRules.replace("{idCardNo}", map.get("userIdCardNumber") != null ? map.get("userIdCardNumber") : "");
        namingRules = namingRules.replace("{nation}", map.get("userNation") != null ? map.get("userNation") : "");
        namingRules = namingRules.replace("{sex}", map.get("userSex") != null ? map.get("userSex") : "");
        namingRules = namingRules.replace("{born}", map.get("userBorn") != null ? map.get("userBorn") : "");
        String zmFileTargetPath = "";
        if (toolBean.isIsMerge()) {
            String regEx = "[\\s~·`!！@#￥$%^……&*（()）\\-——\\-_=+【\\[\\]】｛{}｝\\|、\\\\；;：:‘'“”\"，,《<。.》>、/？?]";
            Pattern p = Pattern.compile(regEx);
            Matcher m = p.matcher(namingRules);
            m.replaceAll("_");
            String targetFilePath = toolBean.getFileTargetPath() + File.separator + namingRules + map.get("zmFileTargetPath");
            File targetFile = new File(targetFilePath);
            //若文件存在重命名
            int fileNumber = 1;
            while (targetFile.exists()) {
                String newFilename = namingRules + "(" + fileNumber + ")" + map.get("zmFileTargetPath");
                String parentPath = targetFile.getParent();
                targetFile = new File(parentPath + File.separator + newFilename);
                fileNumber++;
            }
            if (zmFile != null && fmFile != null) {
                File mergeFile = CompositePhotograph.compositePhoto(zmFile, fmFile, targetFile.getName(), Paths.get(targetFile.getPath()));
                zmFileTargetPath = mergeFile.getPath();
            }
        } else {
            if (zmFile != null) {
                zmFileTargetPath = copyFileF(toolBean.getFileTargetPath(), namingRules, map.get("zmIdCardSign"), map.get("zmFileTargetPath"), zmFile);
            }
            if (fmFile != null) {
                String fmFileTargetPath = copyFileF(toolBean.getFileTargetPath(), namingRules, map.get("fmIdCardSign"), map.get("fmFileTargetPath"), fmFile);
            }
        }
        return new IDCardToolTableBean("", map.get("zmFileOriginalPath"), zmFileTargetPath, map.get("userName"), map.get("userAddress"), map.get("userIdCardNumber"), map.get("userNation"), map.get("userSex"), map.get("userBorn"), map.get("zmIdCardSign"), map.get("signOrg"), map.get("expiryDate"), map.get("signDate"), map.get("namingRules"), null);
    }


    /**
     * 合同签名的多线程任务处理方法
     * 使用框架：frokJion 分而治之
     */
    private class FliesHandleTask extends RecursiveTask<Integer> {
        /**
         * 设置阈值
         */
        private final static int THRESHOLD = 4;
        /**
         * 开始统计的下标
         */
        private int fromIndex;
        /**
         * 统计到哪里结束的下标
         */
        private int toIndex;
        /**
         * 文件记录集合
         **/
        List<File> tempList;

        private IDCardToolBean toolBean;

        private AipOcr client;

        private Map<String, List<IDCardToolTableBean>> listMap;

        private int handleType;

        private ProcessBar pb;

        private List<IDCardToolTableBean> tableBeans;

        public FliesHandleTask(List<File> tempList, int fromIndex, int toIndex, IDCardToolBean toolBean, AipOcr client, Map<String, List<IDCardToolTableBean>> listMap, int handleType, ProcessBar pb, List<IDCardToolTableBean> tableBeans) {
            this.fromIndex = fromIndex;
            this.toIndex = toIndex;
            this.tempList = tempList;
            this.toolBean = toolBean;
            this.client = client;
            this.listMap = listMap;
            this.handleType = handleType;
            this.pb = pb;
            this.tableBeans = tableBeans;
        }

        @Override
        protected Integer compute() {
            //如果小于阈值的话 直接执行
            if (toIndex - fromIndex < THRESHOLD) {
                //int count = 0;
                List<File> smallList = tempList.subList(fromIndex, toIndex + 1);
                int sucessCount = filesHandleProcess(handleType, smallList, toolBean, client, listMap, tableBeans, pb);
                return sucessCount;
            } else {
                //如果大于阈值的话,继续拆分
                int mid = (fromIndex + toIndex) / 2;
                FliesHandleTask left = new FliesHandleTask(tempList, fromIndex, mid, toolBean, client, listMap, handleType, pb, tableBeans);
                FliesHandleTask right = new FliesHandleTask(tempList, mid + 1, toIndex, toolBean, client, listMap, handleType, pb, tableBeans);
                invokeAll(left, right);
                return left.join() + right.join();
            }
        }
    }


    private int filesHandleProcess(int handleType, List<File> tempList, IDCardToolBean toolBean, AipOcr client, Map<String, List<IDCardToolTableBean>> listMap, List<IDCardToolTableBean> tableBeans, ProcessBar pb) {

        int sucessCount = 0;

        try {
            if (handleType == 1) {
                for (int i = 0; i < tempList.size(); i++) {

                    sucessCount++;
                    pb.setCurrentNumber();

                    if (tempList.get(i).isFile()) {
                        System.out.println("文     件：" + tempList.get(i));
                        if (isImage(tempList.get(i))) {
                            //调用图片处理逻辑
                            //File[] paramList = tempList.;

                            File[] paramList = new File[tempList.size()];
                            tempList.toArray(paramList);
                            IDCardToolTableBean idCardToolTableBean = null;

                            idCardToolTableBean = PicProcessing(paramList, i, client);

                            String fileName = tempList.get(i).getName();
                            String fileNameStr = fileName.substring(0, fileName.lastIndexOf("."));
                    /*    int xhxIndex = toolBean.getScanningRules().lastIndexOf("_");
                        int xhIndex = toolBean.getScanningRules().lastIndexOf("*");*/
                            String pipeiStr = toolBean.getScanningRules();
                            if (pipeiStr.equals("匹配全部")) {
                                fileNameStr = fileNameStr;
                            } else if (pipeiStr.equals("匹配下划线之前")) {
                                //下划线大于星号  匹配前面
                                int xhxIndex = fileNameStr.lastIndexOf("_");
                                fileNameStr = fileNameStr.substring(0, xhxIndex);
                            } else if (pipeiStr.equals("匹配下划线之后")) {
                                //下划线大于星号  匹配后面
                                int xhxIndex = fileNameStr.lastIndexOf("_");
                                fileNameStr = fileNameStr.substring(xhxIndex + 1);
                            }
                            if (listMap.get(fileNameStr) != null) {
                                listMap.get(fileNameStr).add(idCardToolTableBean);
                            } else {
                                List<IDCardToolTableBean> list = new ArrayList<>();
                                list.add(idCardToolTableBean);
                                listMap.put(fileNameStr, list);
                            }

                            //tableBeans.add(idCardToolTableBean);

                        }

                    }

                }
            } else {

                for (int i = 0; i < tempList.size(); i++) {
                    sucessCount++;
                    pb.setCurrentNumber();

                    if (tempList.get(i).isDirectory()) {
                        //File childFile = new File(toolBean.getFileOriginalPath());
                        File[] childtempList = tempList.get(i).listFiles();
                        System.out.println("该子目录下对象个数：" + childtempList.length);
                        //文件夹处理逻辑
                        System.out.println("文件夹：" + tempList.get(i));
                        //设置当前运行到了第几个
                        List<IDCardToolTableBean> tableBeanList = null;
                        //子文件处理逻辑
                        tableBeanList = new ArrayList<>();
                        for (int j = 0; j < childtempList.length; j++) {
                            System.out.println("文     件：" + childtempList[j]);
                            if (isImage(childtempList[j])) {
                                //调用图片处理逻辑
                                IDCardToolTableBean idCardToolTableBean = PicProcessing(childtempList, j, client);
                                tableBeanList.add(idCardToolTableBean);
                            }
                        }


                        //存储对象进tables里面
                        if (tableBeanList != null) {
                            IDCardToolTableBean idBean = saveBeans(tableBeanList, toolBean);
                            tableBeans.add(idBean);
                        }

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return sucessCount;
        }

    }

}
