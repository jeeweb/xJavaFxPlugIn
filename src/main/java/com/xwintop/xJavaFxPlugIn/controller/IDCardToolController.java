package com.xwintop.xJavaFxPlugIn.controller;

import com.xwintop.xJavaFxPlugIn.model.IDCardToolBean;
import com.xwintop.xJavaFxPlugIn.model.IDCardToolTableBean;
import com.xwintop.xJavaFxPlugIn.services.IDCardToolService;
import com.xwintop.xJavaFxPlugIn.utils.ProcessBar;
import com.xwintop.xJavaFxPlugIn.view.IDCardToolView;
import com.xwintop.xcore.util.javafx.FileChooserUtil;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseButton;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * 身份证小工具处理
 */
public class IDCardToolController extends IDCardToolView {
    private IDCardToolService idCardToolService = new IDCardToolService();
    private IDCardToolBean data;
    private ObservableList<IDCardToolTableBean> tableData = FXCollections.observableArrayList();
    /* private String[] quartzChoiceBoxStrings = new String[] { "简单表达式", "Cron表达式" };*/

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initView();
        initEvent();
    }

    private void initView() {
        data = idCardToolService.loadingConfigure();
        idCardToolService.setToolData(data);
        textFileNamingRules.setText("{name}_{idCardNo}");
        comboBoxScanningRules.getSelectionModel().select("匹配下划线之后");
        checkBoxIsMerge.setSelected(true);

        tableColumnUserName.setCellValueFactory(new PropertyValueFactory<IDCardToolTableBean, String>("userName"));
        tableColumnUserName.setCellFactory(TextFieldTableCell.<IDCardToolTableBean>forTableColumn());
        tableColumnUserName.setOnEditCommit((TableColumn.CellEditEvent<IDCardToolTableBean, String> t) -> {
            t.getRowValue().setUserName(t.getNewValue());
        });
        tableColumnUserAddress.setCellValueFactory(new PropertyValueFactory<IDCardToolTableBean, String>("userAddress"));
        tableColumnUserAddress.setCellFactory(TextFieldTableCell.<IDCardToolTableBean>forTableColumn());
        tableColumnUserAddress.setOnEditCommit((TableColumn.CellEditEvent<IDCardToolTableBean, String> t) -> {
            t.getRowValue().setUserAddress(t.getNewValue());
        });
        tableColumnUserIdCardNumber.setCellValueFactory(new PropertyValueFactory<IDCardToolTableBean, String>("userIdCardNumber"));
        tableColumnUserIdCardNumber.setCellFactory(TextFieldTableCell.<IDCardToolTableBean>forTableColumn());
        tableColumnUserIdCardNumber.setOnEditCommit((TableColumn.CellEditEvent<IDCardToolTableBean, String> t) -> {
            t.getRowValue().setUserIdCardNumber(t.getNewValue());
        });

        tableColumnUserNation.setCellValueFactory(new PropertyValueFactory<IDCardToolTableBean, String>("userNation"));
        tableColumnUserNation.setCellFactory(TextFieldTableCell.<IDCardToolTableBean>forTableColumn());
        tableColumnUserNation.setOnEditCommit((TableColumn.CellEditEvent<IDCardToolTableBean, String> t) -> {
            t.getRowValue().setUserAddress(t.getNewValue());
        });

        tableColumnUserSex.setCellValueFactory(new PropertyValueFactory<IDCardToolTableBean, String>("userSex"));
        tableColumnUserSex.setCellFactory(TextFieldTableCell.<IDCardToolTableBean>forTableColumn());
        tableColumnUserSex.setOnEditCommit((TableColumn.CellEditEvent<IDCardToolTableBean, String> t) -> {
            t.getRowValue().setUserAddress(t.getNewValue());
        });

        tableColumnUserBorn.setCellValueFactory(new PropertyValueFactory<IDCardToolTableBean, String>("userBorn"));
        tableColumnUserBorn.setCellFactory(TextFieldTableCell.<IDCardToolTableBean>forTableColumn());
        tableColumnUserBorn.setOnEditCommit((TableColumn.CellEditEvent<IDCardToolTableBean, String> t) -> {
            t.getRowValue().setUserAddress(t.getNewValue());
        });
        tableColumnSignOrg.setCellValueFactory(new PropertyValueFactory<IDCardToolTableBean, String>("signOrg"));
        tableColumnSignOrg.setCellFactory(TextFieldTableCell.<IDCardToolTableBean>forTableColumn());
        tableColumnSignOrg.setOnEditCommit((TableColumn.CellEditEvent<IDCardToolTableBean, String> t) -> {
            t.getRowValue().setUserAddress(t.getNewValue());
        });

        tableColumnExpiryDate.setCellValueFactory(new PropertyValueFactory<IDCardToolTableBean, String>("expiryDate"));
        tableColumnExpiryDate.setCellFactory(TextFieldTableCell.<IDCardToolTableBean>forTableColumn());
        tableColumnExpiryDate.setOnEditCommit((TableColumn.CellEditEvent<IDCardToolTableBean, String> t) -> {
            t.getRowValue().setUserAddress(t.getNewValue());
        });

        tableColumnSignDate.setCellValueFactory(new PropertyValueFactory<IDCardToolTableBean, String>("signDate"));
        tableColumnSignDate.setCellFactory(TextFieldTableCell.<IDCardToolTableBean>forTableColumn());
        tableColumnSignDate.setOnEditCommit((TableColumn.CellEditEvent<IDCardToolTableBean, String> t) -> {
            t.getRowValue().setUserAddress(t.getNewValue());
        });


        tableColumnFileOriginalPath.setCellValueFactory(new PropertyValueFactory<IDCardToolTableBean, String>("fileOriginalPath"));
        tableColumnFileOriginalPath.setCellFactory(TextFieldTableCell.<IDCardToolTableBean>forTableColumn());
        tableColumnFileOriginalPath.setOnEditCommit((TableColumn.CellEditEvent<IDCardToolTableBean, String> t) -> {
            t.getRowValue().setFileOriginalPath(t.getNewValue());
        });
        tableColumnFileTargetPath.setCellValueFactory(new PropertyValueFactory<IDCardToolTableBean, String>("fileTargetPath"));
        tableColumnFileTargetPath.setCellFactory(TextFieldTableCell.<IDCardToolTableBean>forTableColumn());
        tableColumnFileTargetPath.setOnEditCommit((TableColumn.CellEditEvent<IDCardToolTableBean, String> t) -> {
            t.getRowValue().setFileTargetPath(t.getNewValue());
        });
        tableViewMain.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tableViewMain.setItems(tableData);
    }

    private void initEvent() {
        FileChooserUtil.setOnDrag(textFileOriginalPath, FileChooserUtil.FileType.FILE);
        FileChooserUtil.setOnDrag(textFileTargetPath, FileChooserUtil.FileType.FOLDER);
        tableData.addListener((ListChangeListener.Change<? extends IDCardToolTableBean> tableBean) -> {
            try {
                saveConfigure(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        tableViewMain.setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.SECONDARY) {
                MenuItem menu_Copy = new MenuItem("复制选中行");
                menu_Copy.setOnAction(event1 -> {
                    IDCardToolTableBean tableBean = tableViewMain.getSelectionModel().getSelectedItem();
                    IDCardToolTableBean tableBean2 = new IDCardToolTableBean(tableBean.getPropertys());
                    tableData.add(tableViewMain.getSelectionModel().getSelectedIndex(), tableBean2);
                });
                MenuItem menu_Remove = new MenuItem("删除选中行");
                menu_Remove.setOnAction(event1 -> {
                    deleteSelectRowAction(null);
                });
                MenuItem menu_RemoveAll = new MenuItem("删除所有");
                menu_RemoveAll.setOnAction(event1 -> {
                    tableData.clear();
                });
                tableViewMain.setContextMenu(new ContextMenu(menu_Copy, menu_Remove, menu_RemoveAll));
            }
        });
    }

    @FXML
    private void chooseOriginalPathAction(ActionEvent event) {
        File file = FileChooserUtil.chooseDirectory();
        if (file != null) {
            textFileOriginalPath.setText(file.getPath());
        }
    }

    @FXML
    private void chooseTargetPathAction(ActionEvent event) {
        File file = FileChooserUtil.chooseDirectory();
        if (file != null) {
            textFileTargetPath.setText(file.getPath());
        }
    }

    @FXML
    private void addItemAction(ActionEvent event) {
        new IDCardToolBean(textFileOriginalPath.getText(),
                textFileTargetPath.getText(),
                textOcrAppId.getText(), textOcrApiKey.getText(), textOcrsecretKey.getText(), textFileNamingRules.getText(), checkBoxIsOneDirectory.isSelected(), comboBoxScanningRules.getValue().toString(), checkBoxIsMerge.isSelected());
    }

    @FXML
    private void deleteSelectRowAction(ActionEvent event) {
        tableData.remove(tableViewMain.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void saveConfigure(ActionEvent event) throws Exception {
        new IDCardToolBean(textFileOriginalPath.getText(),
                textFileTargetPath.getText(),
                textOcrAppId.getText(), textOcrApiKey.getText(), textOcrsecretKey.getText(), textFileNamingRules.getText(), checkBoxIsOneDirectory.isSelected(), comboBoxScanningRules.getValue().toString(), checkBoxIsMerge.isSelected());
        idCardToolService.saveConfigure();
    }

    @FXML
    private void idCardRunAction(ActionEvent event) throws Exception {
        ProcessBar pb = new ProcessBar("正在处理...");
        data = new IDCardToolBean(textFileOriginalPath.getText(),
                textFileTargetPath.getText(),
                textOcrAppId.getText(), textOcrApiKey.getText(), textOcrsecretKey.getText(), textFileNamingRules.getText(), checkBoxIsOneDirectory.isSelected(), comboBoxScanningRules.getValue().toString(), checkBoxIsMerge.isSelected());
        idCardToolService.setToolData(data);

        // 调用进度条
        pb.setVisible();
        Thread thread = new Thread(pb);
        thread.start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                List<IDCardToolTableBean> hashMapList = null;
                try {
                    hashMapList = idCardToolService.idCardRunAction(pb);
                    tableData.addAll(hashMapList);
                    tableViewMain.setItems(tableData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();


    }

    @FXML
    private void otherSaveConfigureAction(ActionEvent event) throws Exception {
        new IDCardToolBean(textFileOriginalPath.getText(),
                textFileTargetPath.getText(),
                textOcrAppId.getText(), textOcrApiKey.getText(), textOcrsecretKey.getText(), textFileNamingRules.getText(), checkBoxIsOneDirectory.isSelected(), comboBoxScanningRules.getValue().toString(), false);
        idCardToolService.otherSaveConfigureAction();
    }

    @FXML
    private void loadingConfigureAction(ActionEvent event) {
        data = idCardToolService.loadingConfigureAction();
        if (data != null) {
            textFileOriginalPath.setText(data.getFileOriginalPath());
            textFileTargetPath.setText(data.getFileTargetPath());
            textOcrAppId.setText(data.getOcrAppId());
            textOcrApiKey.setText(data.getOcrApiKey());
            textOcrsecretKey.setText(data.getOcrsecretKey());
            textFileNamingRules.setText(data.getFileNamingRules());
            comboBoxScanningRules.setValue(data.getScanningRules());
            checkBoxIsOneDirectory.setSelected(data.isIsOneDirectory());

        }

    }

    @FXML
    private void excelExportAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("excel files (*.xls)", "*.xls");
        fileChooser.getExtensionFilters().add(extFilter);
        Stage s = new Stage();
        //Show save file dialog
        File file = fileChooser.showSaveDialog(s);
        if (file == null) {
            //如果是空跳出
            return;
        }
        try {
            FileOutputStream fileOut = new FileOutputStream(file);
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet worksheet = workbook.createSheet("POI Worksheet");

            // index from 0,0... cell A1 is cell(0,0)
            HSSFRow row1 = worksheet.createRow((short) 0);

            HSSFCell cellA1 = row1.createCell((short) 0);
            cellA1.setCellValue("姓名");
            HSSFCellStyle cellStyle = workbook.createCellStyle();
            cellStyle.setFillForegroundColor(HSSFColor.GOLD.index);
            // cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cellA1.setCellStyle(cellStyle);

            HSSFCell cellB1 = row1.createCell((short) 1);
            cellB1.setCellValue("身份证号");
            cellStyle = workbook.createCellStyle();
            cellStyle.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
            //cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cellB1.setCellStyle(cellStyle);

            HSSFCell cellC1 = row1.createCell((short) 2);
            cellC1.setCellValue("性别");
            cellStyle = workbook.createCellStyle();
            cellStyle.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
            // cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cellC1.setCellStyle(cellStyle);


            HSSFCell cellD1 = row1.createCell((short) 3);
            cellD1.setCellValue("民族");
            cellStyle = workbook.createCellStyle();
            cellStyle.setFillForegroundColor(HSSFColor.LIGHT_ORANGE.index);
            //cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cellD1.setCellStyle(cellStyle);

            HSSFCell cellE1 = row1.createCell((short) 4);
            cellE1.setCellValue("出生日期");
            cellStyle = workbook.createCellStyle();
            cellStyle.setFillForegroundColor(HSSFColor.LIGHT_TURQUOISE.index);
            // cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cellE1.setCellStyle(cellStyle);

            HSSFCell cellF1 = row1.createCell((short) 5);
            cellF1.setCellValue("家庭住址");
            cellStyle = workbook.createCellStyle();
            cellStyle.setFillForegroundColor(HSSFColor.LIGHT_TURQUOISE.index);
            // cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cellF1.setCellStyle(cellStyle);

            HSSFCell cellF2 = row1.createCell((short) 6);
            cellF2.setCellValue("签发机关");
            cellStyle = workbook.createCellStyle();
            cellStyle.setFillForegroundColor(HSSFColor.LIGHT_TURQUOISE.index);
            // cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cellF2.setCellStyle(cellStyle);

            HSSFCell cellF3 = row1.createCell((short) 7);
            cellF3.setCellValue("签发日期");
            cellStyle = workbook.createCellStyle();
            cellStyle.setFillForegroundColor(HSSFColor.LIGHT_TURQUOISE.index);
            // cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cellF3.setCellStyle(cellStyle);


            HSSFCell cellF4 = row1.createCell((short) 8);
            cellF4.setCellValue("失效日期");
            cellStyle = workbook.createCellStyle();
            cellStyle.setFillForegroundColor(HSSFColor.LIGHT_TURQUOISE.index);
            // cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cellF4.setCellStyle(cellStyle);

            HSSFCell cellG1 = row1.createCell((short) 9);
            cellG1.setCellValue("图片原路径");
            cellStyle = workbook.createCellStyle();
            cellStyle.setFillForegroundColor(HSSFColor.LIGHT_TURQUOISE.index);
            // cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cellG1.setCellStyle(cellStyle);

            HSSFCell cellH1 = row1.createCell((short) 10);
            cellH1.setCellValue("存储路径");
            cellStyle = workbook.createCellStyle();
            cellStyle.setFillForegroundColor(HSSFColor.LIGHT_TURQUOISE.index);
            // cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cellH1.setCellStyle(cellStyle);

            for (int i = 0; i < tableData.size(); i++) {
                HSSFRow row2 = worksheet.createRow((short) i + 1);
                row2.createCell(0).setCellValue(((IDCardToolTableBean) tableData.get(i)).getUserName());
                row2.createCell(1).setCellValue(((IDCardToolTableBean) tableData.get(i)).getUserIdCardNumber());
                row2.createCell(2).setCellValue(((IDCardToolTableBean) tableData.get(i)).getUserSex());
                row2.createCell(3).setCellValue(((IDCardToolTableBean) tableData.get(i)).getUserNation());
                row2.createCell(4).setCellValue(((IDCardToolTableBean) tableData.get(i)).getUserBorn());
                row2.createCell(5).setCellValue(((IDCardToolTableBean) tableData.get(i)).getUserAddress());
                row2.createCell(6).setCellValue(((IDCardToolTableBean) tableData.get(i)).getSignOrg());
                row2.createCell(7).setCellValue(((IDCardToolTableBean) tableData.get(i)).getSignDate());
                row2.createCell(8).setCellValue(((IDCardToolTableBean) tableData.get(i)).getExpiryDate());
                row2.createCell(9).setCellValue(((IDCardToolTableBean) tableData.get(i)).getFileOriginalPath());
                row2.createCell(10).setCellValue(((IDCardToolTableBean) tableData.get(i)).getFileTargetPath());
            }

            workbook.write(fileOut);
            fileOut.flush();
            fileOut.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

}