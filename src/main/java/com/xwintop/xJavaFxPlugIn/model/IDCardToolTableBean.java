package com.xwintop.xJavaFxPlugIn.model;

import javafx.beans.property.SimpleStringProperty;

import java.io.File;

public class IDCardToolTableBean {

    //文件存在路径
    private SimpleStringProperty fileOriginalPath;
    //文件存储路径
    private SimpleStringProperty fileTargetPath;
    //用户姓名
    private SimpleStringProperty userName;
    //用户地址
    private SimpleStringProperty userAddress;
    //用户身份证
    private SimpleStringProperty userIdCardNumber;
    //行序号
    private SimpleStringProperty lineNumber;
    //民族
    private SimpleStringProperty userNation;
    //性别
    private SimpleStringProperty userSex;
    //出生
    private SimpleStringProperty userBorn;
    //命名规则
    private SimpleStringProperty namingRules;
    //身份证标记 正反标记 fron back
    private SimpleStringProperty idCardSign;
    //签发机关
    private SimpleStringProperty signOrg;
    //失效日期
    private SimpleStringProperty expiryDate;
    //签发日期
    private SimpleStringProperty signDate;
    //源文件
    private File originalFile;

    public IDCardToolTableBean(String lineNumber, String fileOriginalPath, String fileTargetPath, String userName, String userAddress, String userIdCardNumber, String userNation, String userSex, String userBorn, String idCardSign, String signOrg, String expiryDate, String signDate, String namingRules, File originalFile) {
        super();
        this.fileOriginalPath = new SimpleStringProperty(fileOriginalPath);
        this.fileTargetPath = new SimpleStringProperty(fileTargetPath);
        this.userName = new SimpleStringProperty(userName);
        this.userAddress = new SimpleStringProperty(userAddress);
        this.userIdCardNumber = new SimpleStringProperty(userIdCardNumber);
        this.lineNumber = new SimpleStringProperty(lineNumber);
        this.userNation = new SimpleStringProperty(userNation);
        this.userSex = new SimpleStringProperty(userSex);
        this.userBorn = new SimpleStringProperty(userBorn);
        this.namingRules = new SimpleStringProperty(namingRules);
        this.idCardSign = new SimpleStringProperty(idCardSign);
        this.signOrg = new SimpleStringProperty(signOrg);
        this.expiryDate = new SimpleStringProperty(expiryDate);
        this.signDate = new SimpleStringProperty(signDate);
        this.originalFile = originalFile;
    }

    public IDCardToolTableBean(String propertys) {
        String[] strings = propertys.split("__", 13);
        this.fileOriginalPath = new SimpleStringProperty(strings[0]);
        this.fileTargetPath = new SimpleStringProperty(strings[1]);
        this.userName = new SimpleStringProperty(strings[2]);
        this.userAddress = new SimpleStringProperty(strings[3]);
        this.userIdCardNumber = new SimpleStringProperty(strings[4]);
        this.userNation = new SimpleStringProperty(strings[5]);
        this.userSex = new SimpleStringProperty(strings[6]);
        this.userBorn = new SimpleStringProperty(strings[7]);
        this.namingRules = new SimpleStringProperty(strings[8]);
        this.idCardSign = new SimpleStringProperty(strings[9]);
        this.signOrg = new SimpleStringProperty(strings[10]);
        this.expiryDate = new SimpleStringProperty(strings[11]);
        this.signDate = new SimpleStringProperty(strings[12]);
    }

    public String getPropertys() {
        return fileOriginalPath.get() + "__" + fileTargetPath.get() + "__"
                + userName.get() + "__" + userAddress.get() + "__" + userIdCardNumber.get() + "__" + userNation.get() + "__" + userSex.get() + "__" + userBorn.get() + "__" + namingRules.get() + "__" + idCardSign.get() + "__" + signOrg.get() + "__" + expiryDate.get() + "__" + signDate.get();
    }

    public String getFileOriginalPath() {
        return fileOriginalPath.get();
    }

    public SimpleStringProperty fileOriginalPathProperty() {
        return fileOriginalPath;
    }

    public void setFileOriginalPath(String fileOriginalPath) {
        this.fileOriginalPath.set(fileOriginalPath);
    }

    public String getFileTargetPath() {
        return fileTargetPath.get();
    }

    public SimpleStringProperty fileTargetPathProperty() {
        return fileTargetPath;
    }

    public void setFileTargetPath(String fileTargetPath) {
        this.fileTargetPath.set(fileTargetPath);
    }

    public String getUserName() {
        return userName.get();
    }

    public SimpleStringProperty userNameProperty() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName.set(userName);
    }

    public String getUserAddress() {
        return userAddress.get();
    }

    public SimpleStringProperty userAddressProperty() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress.set(userAddress);
    }

    public String getUserIdCardNumber() {
        return userIdCardNumber.get();
    }

    public SimpleStringProperty userIdCardNumberProperty() {
        return userIdCardNumber;
    }

    public void setUserIdCardNumber(String userIdCardNumber) {
        this.userIdCardNumber.set(userIdCardNumber);
    }

    public String getLineNumber() {
        return lineNumber.get();
    }

    public SimpleStringProperty lineNumberProperty() {
        return lineNumber;
    }

    public void setLineNumber(String lineNumber) {
        this.lineNumber.set(lineNumber);
    }

    public String getUserNation() {
        return userNation.get();
    }

    public SimpleStringProperty userNationProperty() {
        return userNation;
    }

    public void setUserNation(String userNation) {
        this.userNation.set(userNation);
    }

    public String getUserSex() {
        return userSex.get();
    }

    public SimpleStringProperty userSexProperty() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex.set(userSex);
    }

    public String getUserBorn() {
        return userBorn.get();
    }

    public SimpleStringProperty userBornProperty() {
        return userBorn;
    }

    public void setUserBorn(String userBorn) {
        this.userBorn.set(userBorn);
    }


    public String getIdCardSign() {
        return idCardSign.get();
    }

    public SimpleStringProperty idCardSignProperty() {
        return idCardSign;
    }

    public void setIdCardSign(String idCardSign) {
        this.idCardSign.set(idCardSign);
    }

    public String getSignOrg() {
        return signOrg.get();
    }

    public SimpleStringProperty signOrgProperty() {
        return signOrg;
    }

    public void setSignOrg(String signOrg) {
        this.signOrg.set(signOrg);
    }

    public String getExpiryDate() {
        return expiryDate.get();
    }

    public SimpleStringProperty expiryDateProperty() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate.set(expiryDate);
    }

    public String getSignDate() {
        return signDate.get();
    }

    public SimpleStringProperty signDateProperty() {
        return signDate;
    }

    public void setSignDate(String signDate) {
        this.signDate.set(signDate);
    }

    public String getNamingRules() {
        return namingRules.get();
    }

    public SimpleStringProperty namingRulesProperty() {
        return namingRules;
    }

    public void setNamingRules(String namingRules) {
        this.namingRules.set(namingRules);
    }

    public File getOriginalFile() {
        return originalFile;
    }

    public void setOriginalFile(File originalFile) {
        this.originalFile = originalFile;
    }
}
